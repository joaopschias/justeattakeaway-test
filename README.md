# Just Eat Takeaway Test
The goal of this test is to implement a sample project, where you visualize a restaurant list.

## Requirements
* [Git](https://git-scm.com/)
* [Docker](https://www.docker.com/)

## Installation
1 - First, clone repository:

```
$ git clone git@gitlab.com:joaopschias/justeattakeaway-test.git
$ cd justeattakeaway-test
```

2 - If you need to develop some code, switch to the branch develop:

*This step is not required and you can test the project using the branch master*

```
$ git checkout develop
$ git pull
```

3 - Create a .env file:

```
$ cp .env.example .env
```

4 - Initialize Docker:
*This step can take a few minutes to be done*

```
$ docker-compose up -d
```

5 - Install/Update Composer packages:

```
$ docker-compose run app composer install
```
or
```
$ docker-compose run app composer update
```

6 - Generate Lumen project key:

The next thing you should do after installing Lumen is set your application key to a random string. Typically, this string should be 32 characters long. The key can be set in the .env environment file.

*If the application key is not set, your user encrypted data will not be secure!*

```
$ docker-compose run app php -r "echo substr(md5(rand()), 0, 32);" 
```

Then you can then copy/paste the output key into the .env file on the APP_KEY variable. 

7 -  Create tables and fill with example values:

```
$ docker-compose run app php artisan migrate --seed
```
or
```
$ docker-compose run app php artisan migrate:refresh --seed
```
## Accessing the Docker server

Server: http://0.0.0.0:8081/

phpMyAdmin: http://0.0.0.0:8181/

## Testing
If you need to run some test in the project use:
```
$ docker-compose run app vendor/bin/phpunit
```

## Extras
Command to clear composer autoload:

```
$ docker-compose exec app composer dump-autoload
```

Command to clear application cache:

```
$ docker-compose exec app php artisan cache:clear
```

Command to stop Docker and clear all containers:

```
$ docker-compose down
$ docker rm $(docker ps -a -q); docker rmi $(docker images -q);
```

## Framework
[Lumen 7](https://lumen.laravel.com/docs/7.x)

## Authors

* **João P. Schias** - [LinkedIn](https://www.linkedin.com/in/joaopschias) - [GitLab](https://gitlab.com/joaopschias)
