<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Restaurant
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property string status
 * @property string sortingValues
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 */
class Restaurant extends Model
{
    public $table = 'restaurants';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = [
    ];

    public $fillable = [
        'name',
        'status',
        'sortingValues',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'status' => 'string',
        'sortingValues' => 'array',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    /**
     * Validation rules labels
     *
     * @var array
     */
    public static $rules_labels = [
    ];

    /**
     * @param [type] $query
     * @return mixed $query
     */
    public function scopeOrderByStatus($query)
    {
        return $query->orderByRaw('FIELD(status, "open", "order ahead", "closed")');
    }

    /**
     * @param [type] $query
     * @return mixed $query
     */
    public function scopeOrderByFavorites($query)
    {
        return $query->orderBy('favorite', 'DESC');
    }

    /**
     * @param [type] $query
     * @param string $term
     * @return mixed $query
     */
    public function scopeSearch($query, string $term)
    {
        return $query->where('name', 'LIKE', '%' . $term . '%');
    }
}
