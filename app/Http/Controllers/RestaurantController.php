<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the Category.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request): array
    {
        $params = $request->all();

        $restaurants = Restaurant::query();

        if (!empty($params['search'])) {
            $restaurants->search($params['search']);
        }

        if (!empty($params['favorites'])) {
            $restaurants->select(DB::raw('`id`, `name`, `status`, IF(`id` IN (' . $params['favorites'] . '), 1, 0) as favorite, `sortingValues`'))->orderByFavorites();
        }

        $restaurants->orderByStatus();

        if (!empty($params['orderBy'])) {
            $orderBy = $params['orderBy'];
            $sortedBy = ($params['sortedBy'] === 'ASC' ? 'ASC' : 'DESC');
            $restaurants->orderBy('sortingValues->' . $orderBy, $sortedBy);
        }

        $restaurants = $restaurants->get();

        return [
            'restaurants' => $restaurants,
        ];
    }
}
