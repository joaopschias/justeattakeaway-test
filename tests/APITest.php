<?php;

class APITest extends TestCase
{
    /**
     * @return void
     */
    public function testAssertSuccess()
    {
        $response = $this->call('GET', '/');

        $this->assertEquals(200, $response->status());
    }

    /**
     * @return void
     */
    public function testAssertFail()
    {
        $response = $this->call('POST', '/');

        $this->assertEquals(405, $response->status());
    }
}
